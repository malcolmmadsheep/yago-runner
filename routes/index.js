'use strict';

var express = require('express');
const yago = require('../miner/YagoMiner');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let word = req.query.q;

  if (word) {
    yago(word)
      .then(data => res.json(data));
  } else {
    return res.json({});
  }
});

module.exports = router;