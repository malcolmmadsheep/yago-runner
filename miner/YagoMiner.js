'use strict';

const pg = require('pg');
const Pool = pg.Pool;
const poolingConfig = {
  user: 'mlclmdsh33p',
  password: '1',
  database: 'yago',
  max: 50
};
const pool = new Pool(poolingConfig);

pool.on('error', (err, client) => {
  console.log('Error occured with yago db: %s, %s', err.message, err.stack);
});

function mine(word) {
  let _normalizedWord = normalizeWord(word);

  return new Promise((resolve, reject) => {
      pool.connect((err, client, done) => {
        if (err) {
          console.log('Error from YAGO MINER:', err);

          return reject(err);
        }

        let queryConfig = {
          text: 'SELECT * FROM yagoFacts WHERE subject = $1;',
          values: [_normalizedWord]
        };

        client.query(queryConfig, (err, pgResult) => {
          if (err) {
            console.log('ERROR YAGO QUERY:', err);
            done();
            
            return resolve({});
          }

          let result = {};
          let allData = {
            other: result
          };
          let rowCount = pgResult.rowCount;

          if (rowCount === 0) {
            done();
            return resolve(allData);
          }

          let rows = pgResult.rows;
          let wikicats = getWikiCats(rows);
          if (wikicats.length) {
            result.wikicats = wikicats;
            allData.definition = wikicats[0];
          }

          let wordnets = getWordnets(rows);
          if (wordnets.length) {
            result.wordnets = wordnets;
          }

          done();

          return resolve(allData);
        })
      })
    })
    .catch((err) => {
      console.log('Error: ', err);

      done();
      return resolve({});
    });
}

function filterRowsByParameter(rows, parameter) {
  let filteredRows = rows.map((row) => (splitYagoObject(row.object)))
    .filter((parameters) => (parameters[0] === parameter))
    .map((parameters) => (parameters.slice(1)));

  return filteredRows;
}

function getWikiCats(rows) {
  let wikicats = filterRowsByParameter(rows, 'wikicat')
    .map((parameters) => (parameters.slice(0).join(' ')));

  return wikicats;
}

function getWordnets(rows) {
  let wordnets = filterRowsByParameter(rows, 'wordnet')
    .map((parameters) => {
      let paramsLength = parameters.length;
      let domains = parameters.slice(0, paramsLength - 1);
      let wnId = parameters[paramsLength - 1];

      return {
        domains: domains,
        id: wnId
      };
    });

  return wordnets;
}

function splitYagoObject(object) {
  let _object = object.substring(1, object.length - 1);

  return _object.split('_');
}

function normalizeWord(subject) {
  let _subject = (typeof subject === 'string') ? subject : '';
  _subject = capitalizeFirstLetter(_subject);
  _subject = _subject.split(' ').map((piece) => (capitalizeFirstLetter(_subject))).join('_');
  _subject = '<' + _subject + '>';

  return _subject;
}


function capitalizeFirstLetter(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
};

function test(word) {
  mine(word)
    .then(r => {
      console.log(r);

      process.exit(0);
    });
}

// test('Coca-Cola');

module.exports = mine;